//
//  PageTableViewCell.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PageTableViewCell: UITableViewCell {

    @IBOutlet weak var factsStackView: UIStackView!
    @IBOutlet weak var refrenceLbl: UILabel!
    
    private let shareBtnWidth = CGFloat(65)
    
    private var facts = [Fact]()
    
    private let disposeBag = DisposeBag()
    
    func setData(page:Page,shareAction: @escaping (String) -> Void){
        self.refrenceLbl.text = "\(page.current_page ?? 0)"
        self.facts = page.data ?? []
        self.factsStackView.arrangedSubviews.forEach { (v) in
            v.isHidden = true
            v.removeFromSuperview()
        }
        for i in 0..<facts.count{
            self.factsStackView.addArrangedSubview(getFactViewFor(index: i,shareAction: shareAction))
        }
    }
    
    private func getFactViewFor(index:Int,shareAction: @escaping (String) -> Void) -> UIView {
        let fact = facts[index]
    
        let horizontalStack = UIStackView()
        horizontalStack.axis = .horizontal
        horizontalStack.distribution = .fill
        
        let verticalStack = UIStackView(frame: CGRect(x: 0, y: 0, width: shareBtnWidth, height: 25))
        verticalStack.axis = .vertical
        verticalStack.distribution = .fill
        
        let numLabel = UILabel(frame: CGRect(x: 0, y: 0, width: shareBtnWidth, height: 25))
        numLabel.numberOfLines = 0
        numLabel.lineBreakMode = .byWordWrapping
        numLabel.contentMode = .center
        numLabel.text = "\(index+1)"
        
        let shareBtn = UIButton(frame: CGRect(x: 0, y: 0, width: shareBtnWidth, height: 25))
        shareBtn.setTitleColor(UIColor.blue, for: .normal)
        shareBtn.setTitle(L10n.share, for: .normal)
        shareBtn.setTitleColor(UIColor.darkPink, for: .normal)
        
        shareBtn.rx.tap.bind {
            shareAction(fact.fact!)
        }.disposed(by: disposeBag)
        
        verticalStack.addArrangedSubview(numLabel)
        verticalStack.addArrangedSubview(shareBtn)
        
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = fact.fact
        
        
        
        horizontalStack.addArrangedSubview(verticalStack)
        horizontalStack.addConstraint(NSLayoutConstraint(item: verticalStack, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: shareBtnWidth))
        
        horizontalStack.addArrangedSubview(label)
        
        return horizontalStack
    }
    
}

