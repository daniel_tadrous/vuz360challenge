//
//  PagesViewModel.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift

class PagesViewModel{
    
    private var service: IPagesService
    private let disposeBag = DisposeBag()
    
    var pagesLinkedList: PagesLinkedList!
    
    let limit = Variable<Int?>(nil)
    let initialLimit = 10
    
    init(service: IPagesService) {
        self.service = service
        self.pagesLinkedList = PagesLinkedList(service: self.service, limit: initialLimit)
        self.limit.asObservable().subscribe(onNext: { [unowned self](limit) in
            if let limit = limit{
                self.pagesLinkedList.limit.value = limit
            }
        }).disposed(by: disposeBag)
    }
   
    func shareText(presentingVC: UIViewController, text:String){
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = presentingVC.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ ]
        
        // present the view controller
        presentingVC.present(activityViewController, animated: true, completion: nil)

    }
}
