//
//  PagesLinkedList.swift
//  VUZ360
//
//  Created by innuva on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift

class PagesLinkedList{
    
    private var head: Page!
    private var tail: Page!
    private var service: IPagesService
    private let disposeBag = DisposeBag()
    //a different dispose bag to cancel any request in progress
    private var disposeBagForRequest = DisposeBag()
    let isInprogress = Variable(false)
    var limit = Variable<Int?>(nil)
    var length = Variable(0)
    
    init(service: IPagesService,limit: Int) {
        self.service = service
        self.limit.asObservable().subscribe(onNext: { [unowned self](limit) in
            if limit != nil{
                self.reset()
            }
        }).disposed(by: disposeBag)
        self.limit.value = limit
        //self.loadNextPage()
    }
    func toList()->[Page]{
        var pages = [Page]()
        var node = head
        while node != nil {
            pages.append(node!)
            node = node?.next
        }
        return pages
    }
    
    func loadNextPage(){
        if !isInprogress.value{
            var strUrl = self.service.firstPageUrl
            if tail != nil{
                strUrl = tail.next_page_url
            }
            if let url = strUrl{
                isInprogress.value = true
                self.service.getPage(url: url, limit: limit.value!).throttle(1.0, scheduler: ConcurrentDispatchQueueScheduler(qos: .background)).subscribe(onNext: { [unowned self](page) in
                    if let page = page {
                        if self.head == nil{
                            self.head = page
                            self.tail = page
                            self.length.value = self.length.value + 1
                        }else{
                            self.tail.next = page
                            self.tail = self.tail.next
                            self.length.value = self.length.value + 1
                        }
                    }
                    },onCompleted:{ [unowned self] in
                        self.isInprogress.value = false
                }).disposed(by: disposeBagForRequest)
            }
        }
    }
    // resets the linked list to initial condition
    private func reset(){
        self.disposeBagForRequest = DisposeBag()
        self.head = nil
        self.tail = nil
        length.value = 0
        isInprogress.value = false
        loadNextPage()
    }
}
