//
//  PagesViewController.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class PagesViewController: UIViewController, PagesViewControllerStoryboardLoadable {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var pagesTableView: UITableView!
    let cellIdentifier = "PageTableViewCell"
    var viewModel: PagesViewModel!
    private let disposeBag = DisposeBag()
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pagesTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        slider.rx.value.subscribe(onNext:{ [unowned self] (value) in
            self.viewModel.limit.value = Int(value)
        }).disposed(by: disposeBag)
        
        viewModel.pagesLinkedList.length.asDriver().drive(onNext: { [unowned self](length) in
            self.pagesTableView.reloadData()
            
        }).disposed(by: disposeBag)
        
        viewModel.pagesLinkedList.isInprogress.asDriver().drive(onNext: { [unowned self] (isLoading) in
                self.activityIndicator.isHidden = !isLoading
        }).disposed(by: disposeBag)
    }
    
}
extension PagesViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let page = self.viewModel.pagesLinkedList.toList()[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PageTableViewCell
        cell.setData(page: page,shareAction: {(text) in
          self.viewModel.shareText(presentingVC: self, text: text)
        })
        
        
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if pagesTableView.visibleCells.count > 0{
            let row = pagesTableView.indexPath(for: pagesTableView.visibleCells.last!)?.row
            if let row = row{
                if row == self.viewModel.pagesLinkedList.length.value - 1{
                    self.viewModel.pagesLinkedList.loadNextPage()
                }
            }
        }
    }
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.pagesLinkedList.length.value
    }
}

