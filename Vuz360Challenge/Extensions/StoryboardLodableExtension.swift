//
//  StoryboardLodableExtension.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit


protocol PagesViewControllerStoryboardLoadable: StoryboardLodable {
}

extension PagesViewControllerStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "Pages"
    }
}
