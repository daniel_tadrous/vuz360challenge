//
//  TextStrings.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation


enum L10n {
    
    // name for general table
    private static let t1 = "Localizable"
    static var share:String {
        return L10n.tr(t1,"Share")
        
    }
    static func getStringFor(key: String) -> String {
        return L10n.tr(t1,key)
        
    }
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
