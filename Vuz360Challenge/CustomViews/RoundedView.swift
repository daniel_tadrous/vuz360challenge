//
//  RoundedView.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class RoundedView: UIView{
    
    @IBInspectable
    var Radius: CGFloat = 0{
        didSet{
            self.addRoundCorners(radious: self.Radius)
        }
    }
    @IBInspectable
    var AllRounded: Bool = false{
        didSet{
            self.addRoundCorners(radious: self.Radius)
        }
    }
    @IBInspectable
    var HasBorder: Bool = false{
        didSet{
            if self.HasBorder{
                self.layer.borderWidth = 1
            }
        }
    }
    @IBInspectable
    var BorderColor: UIColor = UIColor.clear{
        didSet{
            if self.HasBorder{
                self.layer.borderColor = self.BorderColor.cgColor
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
    }
    
    
    func addRoundCorners(radious:CGFloat){
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radious
            if self.AllRounded{
                 self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }else{
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
        } else {
            // Fallback on earlier versions
            self.layer.cornerRadius = radious
        }
    }
}
