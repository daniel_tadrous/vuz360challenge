//
//  Page.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//


import Foundation
import ObjectMapper
import RxSwift

class Page : Mappable {
	
    var total : Int?
	var per_page : String?
	var current_page : Int?
	var last_page : Int?
	var next_page_url : String?
	var prev_page_url : String?
	var from : Int?
	var to : Int?
	var data : [Fact]?

    var next: Page?
    var prev: Page?
    var service: IPagesService?
    
    let disposeBag = DisposeBag()
    
    required init?(map: Map) {
        DispatchQueue.main.async {
            self.service = (UIApplication.shared.delegate as! AppDelegate).container.resolve(IPagesService.self)            
        }
	}

    func mapping(map: Map) {
		total <- map["total"]
		per_page <- map["per_page"]
		current_page <- map["current_page"]
		last_page <- map["last_page"]
		next_page_url <- map["next_page_url"]
		prev_page_url <- map["prev_page_url"]
		from <- map["from"]
		to <- map["to"]
		data <- map["data"]
	}

    func loadNext(limit:Int){
        self.service?.getPage(url: self.next_page_url ?? "", limit: limit).subscribe(onNext: { [unowned self] (page) in
            if let page = page {
                self.next = page
                self.next?.prev = self
            }
        }).disposed(by: disposeBag)
    }
}
