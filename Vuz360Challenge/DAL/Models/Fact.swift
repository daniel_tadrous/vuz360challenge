//
//  Fact.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct Fact : Mappable {
	var fact : String?
	var length : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		fact <- map["fact"]
		length <- map["length"]
	}

}
