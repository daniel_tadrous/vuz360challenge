//
//  PagesService.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire

protocol IPagesService{
    /// get the page list
    ///
    /// - Returns: observable of optional fact list
    func getPage(url: String, limit: Int)->Observable<Page?>
    
    /// get first Page url
    ///
    /// - Returns: url string
    var firstPageUrl: String!{get}
}

class PagesService:BaseService, IPagesService{
    var firstPageUrl:String!{
        get{
            return "\(fullPathFor(type: .FACTS))?page=1"
        }
    }
    
    func getPage( url: String , limit: Int) -> Observable<Page?>{
        let strUrl = "\(url)&limit=\(limit)"        
        return getRequest(url: strUrl)
    }
}
