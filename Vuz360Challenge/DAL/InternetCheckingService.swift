//
//  InternetCheckingService.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire


class InternetCheckingService{
    private static var instance: InternetCheckingService!
    static var shared:InternetCheckingService {
        get{
            if instance == nil{
                instance = InternetCheckingService()
            }
            return instance
        }
    }
    let net = NetworkReachabilityManager()
    public var hasInternet:Variable<Bool?>
    
    
    private init(){
        self.hasInternet = Variable(net?.isReachable)
    }
    
    func start(){
        net?.listener = { status in
            switch status {
                
            case .notReachable:
                print("The network is not reachable")
                self.hasInternet.value = false
            case .reachable(.ethernetOrWiFi),.reachable(.wwan), .unknown:
                print("The network is reachable")
                self.hasInternet.value = true
            }
        }
        
        // start listening
        net?.startListening()
    }
}
