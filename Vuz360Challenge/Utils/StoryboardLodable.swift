//
//  StoryboardLodable.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardLodable {
    static var storyboardName: String { get }
}

