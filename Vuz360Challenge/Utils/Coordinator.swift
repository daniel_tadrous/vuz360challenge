//
//  Coordinator.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift

enum ScreensEnum:String{
    case PagesViewController
}


// A clas responsible about the whole app navigation
class Coordinator {
    private let window: UIWindow
    private let container: Container
    private let navigationController : UINavigationController
    static var shared: Coordinator!
    private var currentScreen: ScreensEnum!
    private var disposeBag = DisposeBag()
    init(window: UIWindow, container: Container) {
        self.window = window
        self.container = container
        self.navigationController = UINavigationController()
        self.window.rootViewController = self.navigationController
        Coordinator.shared = self
        InternetCheckingService.shared.start()
        InternetCheckingService.shared.hasInternet.asDriver().drive(onNext: { (hasConnection) in
            if let hasConnection = hasConnection{
                if hasConnection{
                    InternetConnectionView.hide()
                }else{
                    InternetConnectionView.show()
                }
            }
        }).disposed(by: disposeBag)
    }
    
    /// it decides which screen should be opened at the first launch and load it
    func start(){
        _ = load(screenEnum: .PagesViewController)
    }
    
    private func hideNavigationBar(_ hide:Bool = false){
        self.navigationController.navigationBar.isHidden = hide
        self.navigationController.setNavigationBarHidden(hide, animated: false)
    }
    
    func load(screenEnum: ScreensEnum, _ animated: Bool = true)->UIViewController{
        currentScreen = screenEnum
        var vc:UIViewController
        switch screenEnum {
            case .PagesViewController:
                vc = container.resolveViewController(PagesViewController.self)
                navigationController.setViewControllers([vc], animated: animated)
            //add other screens here
        }
        return vc
    }
    
}
