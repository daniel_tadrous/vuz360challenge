//
//  AutoRegisterer.swift
//  VUZ360
//
//  Created by Daniel Tadrous on 10/18/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import Swinject

class AutoRegisterer{
    
    class func register(container:Container) {
        
        // services
        container.autoregister(IPagesService.self, initializer: PagesService.init)
        
        // viewModels
        container.autoregister(PagesViewModel.self, initializer: PagesViewModel.init)
        
        // viewControllers
        container.storyboardInitCompleted(PagesViewController.self) { r, c
            in c.viewModel = r.resolve(PagesViewModel.self)
        }
    
    }
}
